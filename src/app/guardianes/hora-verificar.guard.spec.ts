import { TestBed } from '@angular/core/testing';

import { HoraVerificarGuard } from './hora-verificar.guard';

describe('HoraVerificarGuard', () => {
  let guard: HoraVerificarGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HoraVerificarGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
