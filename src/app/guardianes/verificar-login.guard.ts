import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../servicios/session.service';

@Injectable({
  providedIn: 'root'
})
export class VerificarLoginGuard implements CanActivate {

  constructor(private router: Router,
              private sessionSrv: SessionService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      return new Promise<boolean>((resolve, reject) => {
        this.sessionSrv.checkSession2()
          .then((result: boolean) => {
            if(result) {
              this.router.navigate(['home']);
              resolve(false);
            } else resolve(true);
          })
      });

  }

}
