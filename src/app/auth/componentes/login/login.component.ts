import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/servicios/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm?: FormGroup | null = null;

  constructor(private sessionSrv: SessionService,
              private router: Router) { }

  ngOnInit(): void {
    this.initFormLogin();
  }

  initFormLogin() {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    let user = this.loginForm?.controls['username'].value;
    let password = this.loginForm?.controls['password'].value;
    this.sessionSrv.login(user, password)
      .then((result: boolean) => {
        if(result) {
          this.router.navigate(['home']);
        }
      });
  }

}
