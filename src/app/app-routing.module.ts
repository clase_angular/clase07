import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HoraVerificarGuard } from './guardianes/hora-verificar.guard';
import { VerificarLoginGuard } from './guardianes/verificar-login.guard';
import { VerificarTokenGuard } from './guardianes/verificar-token.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then( module => module.AuthModule),
    canActivate: [VerificarLoginGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( module => module.HomeModule),
    canActivate: [VerificarTokenGuard]
  },
  { path: '**', redirectTo: 'auth' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
