import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private localStorageSrv: LocalStorageService) { }

  login(user: string, password: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      let token = this.getToken(user, password);
      this.localStorageSrv.setSession({ username: user, token: token });
      resolve(true)
    });
  }

  logout() {
    this.localStorageSrv.removeSession();
  }

  checkSession(): number {
    let session = this.localStorageSrv.getSession();
    return (session?.token || '').length;
  }

  checkSession2(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      let session = this.localStorageSrv.getSession();
      if ((session?.token || '').length) resolve(true);
      else resolve(false);
    });
  }

  private getToken(username: string, password: string){
    return 'qwertyuio';
  }

}
