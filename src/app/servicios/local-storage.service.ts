import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private _localStorage: Storage;

  constructor() {
    this._localStorage = localStorage;
  }

  setSession(session: Session) {
    let data = JSON.stringify(session); //string
    this._localStorage.setItem(SESSION, data);
  }

  getSession(): Session {
    let data = this._localStorage.getItem(SESSION) || '{}';
    return JSON.parse(data);
  }

  removeSession() {
    // this._localStorage.removeItem(SESSION)
    this._localStorage.clear();
  }

}

const SESSION = "MY_SESSION"

export interface Session {
  username: string,
  token: string,
}
